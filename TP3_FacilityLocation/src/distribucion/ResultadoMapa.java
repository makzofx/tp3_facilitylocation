package distribucion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.ICoordinate;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Toolkit;


public class ResultadoMapa extends JFrame {


	private JPanel contentPane;
	private JMapViewer mapa;
	private boolean _nombreClientes;
	private ArrayList<MapMarkerDot> _clientes;
	private ArrayList<MapMarkerDot> _PuntosdeDistribucion;
	private Instancia _instancia;
	private Solver _solver;
	private JTable table;

	public ResultadoMapa(Instancia instancia, Solver solver, boolean nombreClientes) {
		setTitle("Facility Location");
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ResultadoMapa.class.getResource("/distribucion/icon.png")));

		_solver = solver;
		_instancia = instancia;
		_nombreClientes = nombreClientes;
		_clientes = new ArrayList<MapMarkerDot>();
		_PuntosdeDistribucion = new ArrayList<MapMarkerDot>();
		
		levantarClientes(); 										// Transforma la lista de clientes y de resultado en
		levantarPuntosdeDistribucion();								//					 MarkerDots para usarse en el mapa
		
		System.out.println();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 771, 576);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 755, 421);
		contentPane.add(panel);

		// Mapa donde se ver�n los resultados
		mapa = new JMapViewer();
		mapa.setBounds(0, 0, 755, 421);
		cargarPuntosAMapa(mapa);
		mapa.setDisplayPosition(centroDeMapa(_clientes), 12);		// centro la vista del mapa en el centro de todos los puntos
		panel.setLayout(null);
		cargarPuntosAMapa(mapa);									// Cargar todos los puntos en el mapa
		panel.add(mapa);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 432, 627, 94);
		contentPane.add(scrollPane);

		// Tabla donde se mostrar�n los resultados
		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Nombre", "Latitud", "Longitud" }));
		scrollPane.setViewportView(table);

		// Boton de exportar a Json
		JButton btnExportar = new JButton("Exportar");
		btnExportar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				exportar();
			}


		});
		btnExportar.setBounds(656, 503, 89, 23);
		contentPane.add(btnExportar);

		cargarTablaDeResultados();									// Carga los resultados en la tabla

		this.setVisible(true);
	}

	// Carga los resultados del algoritmo en la Jtable
	private void cargarTablaDeResultados() {
		ArrayList<CentroDeDistribucion> locales = _solver.getSolucion();
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		for (CentroDeDistribucion local : locales) {
			model.addRow(new Object[] { local.getNombre(), local.getLatitud(), local.getLongitud() });
		}
	}

	// Carga la lista de MapMakerDot en el mapa con distintos colores segun el tipo de punto que sea
	private void cargarPuntosAMapa(JMapViewer mapa2) {

		// Clientes
		for (MapMarkerDot punto : _clientes) {
			punto.getStyle().setColor(Color.BLACK);
			punto.getStyle().setBackColor(Color.orange);
			mapa2.addMapMarker(punto);
		}
		// Centros de Distribucion
		for (MapMarkerDot punto : _PuntosdeDistribucion) {
			punto.getStyle().setColor(Color.BLACK);
			punto.getStyle().setBackColor(Color.magenta);
			mapa2.addMapMarker(punto);
		}

	}

	// Trae la lista de clientes y l atransforma en lista de MapMarkerDot
	private void levantarPuntosdeDistribucion() {
		ArrayList<CentroDeDistribucion> locales = _solver.getSolucion();

		for (CentroDeDistribucion local : locales) {
			_PuntosdeDistribucion
					.add(new MapMarkerDot(local.getNombre(), new Coordinate(local.getLatitud(), local.getLongitud())));
		}
	}
	
	// Trae la lista de CD y l atransforma en lista de MapMarkerDot
	private void levantarClientes() {
		ArrayList<Cliente> clientes = _instancia.getClientes();
		String nombre;
		for (Cliente a : clientes) {
			nombre=_nombreClientes?a.getNombre():"";
			_clientes.add(new MapMarkerDot(nombre, new Coordinate(a.getLatitud(), a.getLongitud())));
		}

	}

	private void exportar() {
		File export;
		
		JFileChooser guardar = new JFileChooser();
		guardar.setSelectedFile(new File("export.json"));
		int resultado = guardar.showSaveDialog(null);
		if (resultado==0) {	
			guardar.setFileSelectionMode(JFileChooser.FILES_ONLY);
			export=guardar.getSelectedFile();
			SerializarResultadoJSON json = new SerializarResultadoJSON(_solver);
			json.guardarJson(json.generarJson(), export);
		}
	}
	
	// Calcula el centro para centrar el mapa
	private ICoordinate centroDeMapa(ArrayList<MapMarkerDot> _lugares) {

		double lat = 0;
		double lon = 0;

		for (int i = 0; i < _lugares.size(); i++) {
			lat += _lugares.get(i).getLat();
			lon += _lugares.get(i).getLon();
		}

		lat /= _lugares.size();
		lon /= _lugares.size();
		Coordinate ret = new Coordinate(lat, lon);
		return ret;
	}
}
