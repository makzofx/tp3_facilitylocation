package distribucion;

public class Locacion {

	// Coordenadas extremas de Argentina
	private transient double _extremoNorte = -21.766;
	private transient double _extremoSur = -55.05;
	private transient double _extremoEste = -53.6333;
	private transient double _extremoOeste = -73.5666;

	private String _nombre;
	private double _lat;
	private double _long;

	public Locacion(String nombre, Double latitud, Double longitud) {

		_nombre = nombre;
		_lat = validarLat(latitud);
		_long = validarLong(longitud);

	}

	
	// Devuelve la distancia en Metros desde una ubicaci�n a otra
	public int distanciaDe(Locacion destino) {

		double lat1 = Math.toRadians(this.getLatitud());
		double lon1 = Math.toRadians(this.getLongitud());
		double lat2 = Math.toRadians(destino.getLatitud());
		double lon2 = Math.toRadians(destino.getLongitud());

		double dlon = (lon2 - lon1);
		double dlat = (lat2 - lat1);

		double sinlat = Math.sin(dlat / 2);
		double sinlon = Math.sin(dlon / 2);

		double a = (sinlat * sinlat) + Math.cos(lat1) * Math.cos(lat2) * (sinlon * sinlon);
		double c = 2 * Math.asin(Math.min(1.0, Math.sqrt(a)));

		double distanciaEnMetros = 6371 * c * 1000;

		return (int) distanciaEnMetros;

	}

	public String getNombre() {
		return _nombre;
	}

	public Double getLongitud() {
		return _long;
	}

	public Double getLatitud() {
		return _lat;
	}

	// Valida si la Latitud est� dentro de los extremos correspondientes a Argentina
	private Double validarLat(Double latitud) {

		if (latitud > this._extremoNorte || latitud < this._extremoSur)
			throw new IllegalArgumentException("Coordenada fuera del rango vlido: " + latitud);

		return latitud;
	}

	// Valida si la Longitud est� dentro de los extremos correspondientes a Argentina
	private Double validarLong(Double longitud) {

		if (longitud > this._extremoEste || longitud < this._extremoOeste)
			throw new IllegalArgumentException("Coordenada fuera del rango vlido: " + longitud);

		return longitud;
	}

	public boolean equals(Locacion obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		
		return ((this.getNombre() == obj.getNombre() && doubleIguales(this.getLatitud(), obj.getLatitud()))
					&& doubleIguales(this.getLongitud(), obj.getLongitud()));
	}


	private boolean doubleIguales(Double a, Double b) {
		return (Double.compare(a,b)==0);
	}



}
