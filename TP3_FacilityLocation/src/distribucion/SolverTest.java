package distribucion;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.util.Comparator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

class SolverTest {

	@Test
	void getLocacionTest() {
		Solver A = ejemplo();
		assertEquals("Jose Cpaz", A.getLocacion(1).getNombre());
	}

	@Test
	void getLocacionExcepcionTest() {
		Solver A = ejemplo();
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> A.getLocacion(-1));
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> A.getLocacion(5));
	}

	@Test
	void agregaDistanciasDeClientesTest() {
		Solver A = ejemplo();
		A.agregaDistanciasDeClientes();

		assertEquals(146029, A.get_PuntosdeDistribucion().get(0).getDistanciaAClientes());
	}

	@Test
	void resolverTestSumaDistanciaAClientes() {
		Solver A = ejemplo();
		A.resolver(new Comparator<CentroDeDistribucion>() {

			@Override
			public int compare(CentroDeDistribucion uno, CentroDeDistribucion otro) {

				return uno.getDistanciaAClientes() - otro.getDistanciaAClientes();
			}
		});

		assertEquals("Jose Cpaz", A.getSolucion().get(1).getNombre());

	}

	@Test
	void resolverTestCantClientesCercanos() {
		Solver A = ejemplo();
		A.resolver(new Comparator<CentroDeDistribucion>() {

			@Override
			public int compare(CentroDeDistribucion uno, CentroDeDistribucion otro) {

				return otro.getQclientesCercanos() - uno.getQclientesCercanos();
			}
		});

		assertEquals("Rotonda GB", A.getSolucion().get(2).getNombre());

	}

	@Test
	void agregarQdeClientesCercanosTest() {
		Solver A = ejemplo();
		A.agregarQdeClientesCercanos();
		assertEquals(8, A.get_PuntosdeDistribucion().get(2).getQclientesCercanos());
	}

	@Test
	void ordernarPorDistanciaAClientesTest() {
		Solver A = ejemplo();
		A.ordenar(A.get_PuntosdeDistribucion(), new Comparator<CentroDeDistribucion>() {

			@Override
			public int compare(CentroDeDistribucion uno, CentroDeDistribucion otro) {

				return uno.getDistanciaAClientes() - otro.getDistanciaAClientes();
			}
		});

		assertEquals("Peron y 202", A.get_PuntosdeDistribucion().get(2).getNombre());
	}

	@Test
	void ordernarPorQClientesTest() {
		Solver A = ejemplo();
		A.ordenar(A.get_PuntosdeDistribucion(), new Comparator<CentroDeDistribucion>() {

			@Override
			public int compare(CentroDeDistribucion uno, CentroDeDistribucion otro) {

				return otro.getQclientesCercanos() - uno.getQclientesCercanos();
			}
		});

		assertEquals("202 y G.Campos", A.get_PuntosdeDistribucion().get(3).getNombre());
	}
	
	@Test
	void agregarCDCercanoAClientesTest() {
		Solver A = ejemplo();
		A.agregarCDCercanoAClientes();
		assertEquals("SanMartin y Marquez", A.get_instancia().getClientes().get(20).getCdMasCercano().getNombre());
	}
	
	private Solver ejemplo() {
		Instancia a = new Instancia(2);
		File Clientes = new File("Clientes.csv");
		File Centros = new File("Locales.csv");
		a.cargarListaClientes(Clientes);
		a.cargarListaLocales(Centros);
		Solver ret = new Solver(a);
		ret.resolver(new Comparator<CentroDeDistribucion>() {

			@Override
			public int compare(CentroDeDistribucion uno, CentroDeDistribucion otro) {

				return uno.getDistanciaAClientes() - otro.getDistanciaAClientes();
			}
		});

		return ret;
	}

}
