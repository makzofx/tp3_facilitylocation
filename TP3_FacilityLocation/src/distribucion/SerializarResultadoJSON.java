package distribucion;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SerializarResultadoJSON {

	@SuppressWarnings("unused")
	private ArrayList<CentroDeDistribucion> LocalesAAbrir;

	public SerializarResultadoJSON(Solver solver) {
		LocalesAAbrir = solver.getSolucion();

	}

	public String generarJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);

		return json;

	}

	public void guardarJson(String jsonAGuardar, File _exportarJson) {

		try {
			FileWriter writer = new FileWriter(_exportarJson);
			writer.write(jsonAGuardar);
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
