package distribucion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

public class Instancia {

	private ArrayList<Cliente> _listaDeClientes;							// Listado de clientes
	private ArrayList<CentroDeDistribucion> _ListaDePosiblesCentros;		// Listado de posibles CD a abrir
	private int _cantidadCentrosAAbrir;										// Cantidad de CD a abrir

	public Instancia(int k) {

		_listaDeClientes = new ArrayList<Cliente>();
		_ListaDePosiblesCentros = new ArrayList<CentroDeDistribucion>();
		set_cantidadLocalesAAbrir(k);

	}

	@SuppressWarnings("unchecked")
	public ArrayList<Cliente> getClientes() {
		return (ArrayList<Cliente>) _listaDeClientes.clone();

	}

	@SuppressWarnings("unchecked")
	public ArrayList<CentroDeDistribucion> getPuntosPosibles() {
		return (ArrayList<CentroDeDistribucion>) _ListaDePosiblesCentros.clone();

	}

	public int get_cantidadLocalesAAbrir() {
		return _cantidadCentrosAAbrir;
	}

	public void set_cantidadLocalesAAbrir(int _cantidadLocalesAAbrir) {
		this._cantidadCentrosAAbrir = _cantidadLocalesAAbrir;
	}

	public void set_clientes(ArrayList<Cliente> _clientes) {
		this._listaDeClientes = _clientes;
	}

	public void set_puntosPosibles(ArrayList<CentroDeDistribucion> _puntosPosibles) {
		this._ListaDePosiblesCentros = _puntosPosibles;
	}

	// Devuelve la cantidad de posibles CD a abrir
	public int cantidadLocales() {
		return _ListaDePosiblesCentros.size();
	}

	// Devuelve la cantidad de clientes
	public int cantidadClientes() {
		return _listaDeClientes.size();
	}

	// Carga la lista de CD a abrir desde un archivo CSV
	public void cargarListaLocales(File archivo) {
	
		BufferedReader bufferLectura2 = null;
		
		try {
			
			bufferLectura2 = new BufferedReader(new FileReader(archivo));
			String linea = bufferLectura2.readLine();

			while (linea != null) {
				String[] campos = linea.split(";");

				String c1 = campos[0];

				double c2 = Double.parseDouble(campos[1]);

				double c3 = Double.parseDouble(campos[2]);

				this._ListaDePosiblesCentros.add( new CentroDeDistribucion(c1, c2, c3));
				linea = bufferLectura2.readLine();
			}
		} catch (

		IOException e) {
			e.printStackTrace();
		}
	}

	// Carga la lista de clientes desde un archivo CSV
	public void cargarListaClientes(File archivoClientes) {
		BufferedReader bufferLectura = null;
		try {

			bufferLectura = new BufferedReader(new FileReader(archivoClientes));
			String linea = bufferLectura.readLine();

			while (linea != null) {
				String[] campos = linea.split(";");


				String c1 = campos[0];
				double c2 = Double.parseDouble(campos[1]);
				double c3 = Double.parseDouble(campos[2]);
				this._listaDeClientes.add( new Cliente(c1, c2, c3));

				linea = bufferLectura.readLine();
			}
		} catch (

		IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public String getNombrecliente (int i) {
		return _listaDeClientes.get(i).getNombre();
		
	}

	public String getNombreDelLocal (int i) {
		return _ListaDePosiblesCentros.get(i).getNombre();
		
	}
	
	
}
