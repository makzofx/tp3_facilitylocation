package distribucion;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class Instancia_Test {

	// creo una instancia de ejemplo
	private Instancia ejemplo() {
		Instancia ret = new Instancia(10);
		File Clientes = new File("Clientes.csv");
		File Centros = new File("Locales.csv");
		ret.cargarListaClientes(Clientes);
		ret.cargarListaLocales(Centros);
		return ret;
	}

	@Test
	public void cantidadClientesTest() {
		Instancia instancia = ejemplo();
		assertEquals(29, instancia.cantidadClientes());
	}

	@Test
	public void CantidadLocalesTest() {
		Instancia instancia = ejemplo();
		assertEquals(5, instancia.cantidadLocales());
	}

	@Test
	public void nombreClienteTest() {
		Instancia instancia = ejemplo();
		assertEquals("Cliente 14", instancia.getNombrecliente(13));
	}

	@Test
	public void nombreLocalTest() {
		Instancia instancia = ejemplo();
		assertEquals("Rotonda GB", instancia.getNombreDelLocal(4));
	}
	
}
