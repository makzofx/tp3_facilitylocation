package distribucion;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LocacionTest {

	@Test
	void rangoLatitudTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> locacionTest(-10.0, -60.0));
		Assertions.assertThrows(IllegalArgumentException.class, () -> locacionTest(-56.0, -60.0));
	}

	@Test
	void rangoLongitudTest() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> locacionTest(-32.0, -50.0));
		Assertions.assertThrows(IllegalArgumentException.class, () -> locacionTest(-32.0, -75.0));
	}

	@Test
	void distanciaTest() {
		Locacion Origen = locacionTest(-34.521723, -58.701151);
		Locacion Destino = locacionTest(-34.509110, -58.728557);
		assertEquals(2876, Origen.distanciaDe(Destino));
	}

	@Test
	void equalsAlliasingTest() {
		Locacion a = locacionTest(-34.521723, -58.701151);
		Locacion b = a;
		assertTrue(a==b);
	}
	
	@Test
	void equalsNullTest() {
		Locacion a = locacionTest(-34.521723, -58.701151);
		Locacion b = null;
		assertFalse(a==b);
	}
	
	@Test
	void equalsIgualesTest() {
		Locacion a = new Locacion("prueba", -34.521723, -58.701151);
		Locacion b = new Locacion("prueba", -34.521723, -58.701151);
		assertTrue(a.equals(b));
	}
	
	
	
	private Locacion locacionTest(double d, double e) {
		Locacion locacion = new Locacion("", d, e);
		return locacion;
	}

}
