package distribucion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Solver {
	
	Instancia _instancia;

	private ArrayList<CentroDeDistribucion> _PuntosdeDistribucion;
	private ArrayList<CentroDeDistribucion> _solucion;

	public Solver(Instancia instancia) {
		_instancia = instancia;
		_PuntosdeDistribucion = new ArrayList<CentroDeDistribucion>();
		_solucion = new ArrayList<CentroDeDistribucion>();
	}
	
	// Este metodo quizas est� de mas, o almenoes esta mal la validacion
	public Locacion getLocacion(int indice) {
		if (indice >= _PuntosdeDistribucion.size() || indice <= 0)
			throw new ArrayIndexOutOfBoundsException("Indice invalido: " + indice);
		return _PuntosdeDistribucion.get(indice);

	}

	@SuppressWarnings("unchecked")
	public ArrayList<CentroDeDistribucion> get_PuntosdeDistribucion() {
		return (ArrayList<CentroDeDistribucion>) _PuntosdeDistribucion.clone();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<CentroDeDistribucion> getSolucion() {
		return (ArrayList<CentroDeDistribucion>) _solucion.clone();
	}

	// Agrega a todos los posibles CD la suma de las distancia de todos los clientes
	public void agregaDistanciasDeClientes() {
		
		ArrayList<Cliente> clientes = _instancia.getClientes();
		ArrayList<CentroDeDistribucion> locales = _instancia.getPuntosPosibles();
		
		for(int i=0; i<locales.size(); i++) {
			int sum=0;
			for(int j=0; j<clientes.size()-1; j++) {
				sum+=locales.get(i).distanciaDe(clientes.get(j));
				
			}
			locales.get(i).setDistanciaAClientes(sum);
		}
	}
	
	// Resuelve segun el comparador que se le pase.
	public void resolver(Comparator<CentroDeDistribucion> comparador) {
		agregaDistanciasDeClientes();
		agregarCDCercanoAClientes();
		agregarQdeClientesCercanos();
		_PuntosdeDistribucion=_instancia.getPuntosPosibles(); // se devuelve un clon
		ordenar(_PuntosdeDistribucion, comparador);
		for(int i=0; i<_instancia.get_cantidadLocalesAAbrir(); i++) {
			_solucion.add(_PuntosdeDistribucion.get(i));
		}

	}
	
	// Agraga a los posibles CD la cantidad de clientes para los que sea ese el CD mas cercano
	public void agregarQdeClientesCercanos() {
		ArrayList<Cliente> clientes = _instancia.getClientes();
		for(Cliente cliente : clientes) {
			cliente.getCdMasCercano().sumarClienteCercano();
		}		
	}

	// Ordena el arrayList segun el comparardo pasado como par�metro
	public void ordenar(ArrayList<CentroDeDistribucion> puntosPosibles, Comparator<CentroDeDistribucion> comparador) {

		Collections.sort(puntosPosibles, comparador);
	
	}

	// Agrega a cada cliente, cual es el posible CD mas cercano
	public void agregarCDCercanoAClientes() {
		ArrayList<Cliente> clientes = _instancia.getClientes();
		ArrayList<CentroDeDistribucion> locales = _instancia.getPuntosPosibles();
		for(Cliente cliente : clientes) {
			CentroDeDistribucion CD = locales.get(0);
			int distancia = cliente.distanciaDe(CD);
			for(CentroDeDistribucion centro : locales) {
				if(cliente.distanciaDe(centro)<distancia) {
					distancia=cliente.distanciaDe(centro);
					CD=centro;
				}
			}
			cliente.setCdMasCercano(CD);
		}
	}

	public Instancia get_instancia() {
		return _instancia;
	}
	
	
	
	
}
