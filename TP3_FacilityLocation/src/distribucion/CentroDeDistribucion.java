package distribucion;

public class CentroDeDistribucion extends Locacion {

	
	private transient int _distanciaAClientes;			// Almacenar� la suma de distancia el CD a todos los clientes
	private transient int _qClientesCercanos;			// Almacenar� la cantidad de clientes para los que somos el CD m�s cercano

	public CentroDeDistribucion(String nombre, Double latitud, Double longitud) {
		super(nombre, latitud, longitud);

	}

	public void setDistanciaAClientes(int i) {
		_distanciaAClientes = i;
	}

	public int getDistanciaAClientes() {
		return _distanciaAClientes;
	}

	public int getQclientesCercanos() {
		return _qClientesCercanos;
	}
	
	public void setQclientesCercanos(int q) {
		_qClientesCercanos = q;
	}

	public void sumarClienteCercano() {
		_qClientesCercanos++;
	}
}
