package distribucion;

import static org.junit.Assert.assertTrue;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.util.Comparator;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.Box;
import java.awt.Component;
import javax.swing.JSeparator;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;

public class Principal extends JFrame {

	private JPanel contentPane;
	private JTextField rutaArchivoClientes;
	private JTextField rutaArchivoLocales;
	private JSpinner cantidadK;
	private File archivoClientes;
	private File archivoLocales;
	private JRadioButton rdbtnSumaDistancia;
	private JRadioButton rdbtnClientesCercanos;
	private JCheckBox mostrarNombreClientes;
	
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public Principal() {
		setTitle("Facility Location");
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Principal.class.getResource("/distribucion/icon.png")));
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (InstantiationException e) {
		
			e.printStackTrace();
		} catch (IllegalAccessException e) {
		
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
		
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 205);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// TextField donde aparece la ruta del archivo de clientes
		rutaArchivoClientes = new JTextField();
		rutaArchivoClientes.setEditable(false);
		rutaArchivoClientes.setBounds(114, 11, 333, 20);
		contentPane.add(rutaArchivoClientes);
		rutaArchivoClientes.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Archivo de Clientes");
		lblNewLabel.setBounds(10, 14, 94, 14);
		contentPane.add(lblNewLabel);
		
		// Boton abrir para lista de clientes
		JButton btnAbrirClientes = new JButton("Abrir");
		btnAbrirClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				archivoClientes = cuadroAbrir(rutaArchivoClientes);
			}


		});
		btnAbrirClientes.setToolTipText("Abra el archivo que contenta la lista de clientes.");
		btnAbrirClientes.setBounds(457, 10, 71, 23);
		contentPane.add(btnAbrirClientes);
		
		JLabel lblNewLabel_1 = new JLabel("Archivo de Locales");
		lblNewLabel_1.setBounds(10, 41, 94, 14);
		contentPane.add(lblNewLabel_1);
		
		// TextField donde aparece la ruta del archivo de CD
		rutaArchivoLocales = new JTextField();
		rutaArchivoLocales.setEditable(false);
		rutaArchivoLocales.setBounds(114, 38, 333, 20);
		contentPane.add(rutaArchivoLocales);
		rutaArchivoLocales.setColumns(10);
		
		// Boton abrir para lista de CD
		JButton btnAbrirLocales = new JButton("Abrir");
		btnAbrirLocales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				archivoLocales = cuadroAbrir(rutaArchivoLocales);
				
			}
		});
		btnAbrirLocales.setToolTipText("Abra el archivo que contenta la lista de los posibles locales a abrir.");
		btnAbrirLocales.setBounds(457, 35, 71, 23);
		contentPane.add(btnAbrirLocales);
		
		// JSpinner para la cantidad de locales a abrir
		cantidadK = new JSpinner();
		cantidadK.setModel(new SpinnerNumberModel(1, 1, null, 1));
		cantidadK.setBounds(150, 69, 43, 20);
		contentPane.add(cantidadK);
		
		JLabel lblNewLabel_2 = new JLabel("Cantidad de locales a abrir");
		lblNewLabel_2.setBounds(10, 72, 136, 14);
		contentPane.add(lblNewLabel_2);
		
		// Boton procesar
		JButton btnProcesar = new JButton("Procesar");
		btnProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(archivosValidos()) {													// Si se elegieron los archivos...
					Instancia instancia = new Instancia((int) cantidadK.getValue());	// Se genera una nueva instancia del problema
					instancia.cargarListaLocales(archivoLocales);						// Cargo lista de locales a abrir
					instancia.cargarListaClientes(archivoClientes);						// Cargo lista de clientes
					Solver solver = new Solver(instancia);								// Se crea una nueva solucion de la instancia
					ejecutarAlgoritmo(solver);											// Se resuelve segun la eleccion del usuario
					ResultadoMapa mapa = new ResultadoMapa(instancia, solver, mostrarNombreClientes.isSelected());// Se carga el Form de resultados
					dispose();															// Se cierra este Form
				}
				else {																	// Si alguno de los archivos es Null, se muestra un alerta
					JOptionPane.showMessageDialog(null, "Debe cargar los dos archivos correspondientes antes de procesar", "Archivo/s inv�lido/s", JOptionPane.ERROR_MESSAGE);
				}
			}

		});
		btnProcesar.setBounds(419, 120, 113, 44);
		contentPane.add(btnProcesar);
		
		// Grupo para los  RoundButtons
		ButtonGroup bg = new ButtonGroup();
		
		// Para elegir el algoritmo a utilizar
		rdbtnSumaDistancia = new JRadioButton("Por suma de distancia a clientes");
		rdbtnSumaDistancia.setSelected(true);
		rdbtnSumaDistancia.setBounds(10, 120, 183, 23);
		contentPane.add(rdbtnSumaDistancia);
		bg.add(rdbtnSumaDistancia);
		
		rdbtnClientesCercanos = new JRadioButton("Por cant. de clientes cercanos");
		rdbtnClientesCercanos.setToolTipText("Se calcula en base a la cantidad de clientes para los cuales se es el centro de distribuci\u00F3n mas cercano.");
		rdbtnClientesCercanos.setBounds(10, 141, 183, 23);
		contentPane.add(rdbtnClientesCercanos);
		bg.add(rdbtnClientesCercanos);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 115, 399, 15);
		contentPane.add(separator);
		
		JLabel lblNewLabel_3 = new JLabel("M\u00E9todo a utilizar");
		lblNewLabel_3.setBounds(10, 102, 94, 14);
		contentPane.add(lblNewLabel_3);
		
		mostrarNombreClientes = new JCheckBox("Mostrar nombre de clientes en mapa");
		mostrarNombreClientes.setBounds(246, 68, 206, 23);
		contentPane.add(mostrarNombreClientes);
		
	}
	
	// Muestra el cuadro de dialogo Abrir para elegir un archivo y lo devuelve. Requiere JText para mostrar la ruta del archivo 
	private File cuadroAbrir(JTextField ruta) {
		File ret = null;
		JFileChooser selectorArchivos = new JFileChooser();										// Crear objeto
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos CSV", "CSV");	// Establecer filtro de tipo de archivo
		selectorArchivos.setFileFilter(filtro);													// Aplicar Filtro
		selectorArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);							// Llama al objeto Abrir archivo
		int resultado = selectorArchivos.showOpenDialog(selectorArchivos);						// 0 si no se eligi� un archivo
		ret = selectorArchivos.getSelectedFile();												// Establece a archivo el archivo abierto
		if (resultado==0) {																		// Si se selecciona un archivo CSV
			ruta.setText(ret.getAbsolutePath());
			
			}
		else {																					// si se cancela la accion de abrir
			JOptionPane.showMessageDialog(null, "Por favor, seleccione un archivo v�lido", "Archivo inv�lido", JOptionPane.ERROR_MESSAGE);
		}
		return ret;
		
	}

	// Revisa si se eligieron ambos archivos.
	private boolean archivosValidos() {
		
		return (archivoClientes!=null && archivoLocales!=null);
	}

	// Ejecuta el algoritmo segun eleccion del usuario
	private void ejecutarAlgoritmo(Solver solver) {
		if(rdbtnSumaDistancia.isSelected()) {
		solver.resolver(new Comparator<CentroDeDistribucion>() {
		
			@Override
			public int compare(CentroDeDistribucion uno, CentroDeDistribucion otro) {
				
				return uno.getDistanciaAClientes() - otro.getDistanciaAClientes();
			}
		});
		}
		if(rdbtnClientesCercanos.isSelected()) {
		solver.resolver(new Comparator<CentroDeDistribucion>() {
		
			@Override
			public int compare(CentroDeDistribucion uno, CentroDeDistribucion otro) {
				
				return otro.getQclientesCercanos() - uno.getQclientesCercanos();
			}
		});
		}
		
		
	}
}
