package distribucion;

public class Cliente extends Locacion {

	private CentroDeDistribucion _cdMasCercano;			// Almacenar� cual es el posible CD m�s cercano
	
	public Cliente(String nombre, Double latitud, Double longitud) {
		super(nombre, latitud, longitud);
	}
	
	public CentroDeDistribucion getCdMasCercano() {
		return _cdMasCercano;
	}

	public void setCdMasCercano(CentroDeDistribucion cdMasCercano) {
		_cdMasCercano = cdMasCercano;
	}
	
}
